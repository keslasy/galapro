import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormGroup, FormControl } from '@angular/forms';
import { Urls } from '../../providers/urlGet';

const expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  gForm: FormGroup;
  sUrl: string;
  isChecked: boolean = false;
  message: string;
  regex: RegExp;  

  constructor(public navCtrl: NavController, public myUrl: Urls) {
    this.gForm = new FormGroup({
       urlIn: new FormControl(),
       chkIn: new FormControl()
    });
  }
  
  onSubmit(formData) {
    this.message = "";
    this.sUrl = formData.urlIn;
    this.isChecked = formData.chkIn;
    if (this.isChecked) {
      this.myUrl.getUrls().then((data) => {
        this.sUrl = data.url;
        window.open(this.sUrl, '_system');
      });
    } else {
      if (!this.sUrl || this.sUrl == undefined || this.sUrl == "" || this.sUrl.length == 0) {
        this.message = "Epmty URL no action was taken";
      } else {
        this.regex = new RegExp(expression);
        if (this.sUrl.match(this.regex)) {
          window.open(this.sUrl, '_system');
        }else {
          this.message = "Please type a valid URL";
        }
      }
    }
  }
  
}   

